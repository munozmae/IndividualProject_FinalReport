package com.example.munozmae.myfirstapp;

import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.icu.text.RelativeDateTimeFormatter;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.content.Context;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by munozmae on 03/10/2016.
 */

public class GamePanel extends SurfaceView implements SurfaceHolder.Callback
{
    public static final int WIDTH=856;
    public static final int HEIGHT=480;
    public static final int MOVESPEED = -5;
    private MainThread thread;
    private Background bg;
    private Player player;
    private Palette palette;
    //private Bird bird;
    private int best;
    private boolean newGameCreated;
    private boolean reset;
    private boolean started;
    private boolean memory;
    private long startReset;
    private long memoryTime;
    private ArrayList<Bird> birds;
    private ArrayList<Paintball> balls;
    private int[] birdTypes = {R.drawable.fat_bird_px_colored, R.drawable.ballon_bird_colored, R.drawable.long_bird_px, R.drawable.long_peack_px_colored, R.drawable.big_head};
    private Random rand = new Random();
    private int onetime=0;
    private long birdStartTime;
    private int color=0;    //Color 0 is red, 1 blue, 2 yellow, 3 green and 4 purple
    private int score=0;
    private int numberofBirds=0;
    private int lives=3;
    private int level=1;
    private int gameover=0;



    public GamePanel(Context context)
    {
        super(context);
        //add the callback to the surfaceholder to intercept events
        getHolder().addCallback(this);


        //make gamePanel focusable so it can handle events
        setFocusable(true);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height){}

    @Override
    public void surfaceDestroyed(SurfaceHolder holder)
    {
        boolean retry = true;
        int counter = 0;
        while(retry && counter<1000) {
            counter++;
            try {
                thread.setRunning(false);
                thread.join();
                retry = false;
                thread = null;

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder)
    {
        bg = new Background(BitmapFactory.decodeResource(getResources(),R.drawable.grassbg1));
        palette=new Palette(BitmapFactory.decodeResource(getResources(), R.drawable.color_pallete));
        //player = new Player(BitmapFactory.decodeResource(getResources(), R.drawable.helicopter), 65, 25, 3);
        player = new Player(BitmapFactory.decodeResource(getResources(), R.drawable.magiccanvas_80x78_2), 80, 78, 1);
        //bird = new Bird(BitmapFactory.decodeResource(getResources(), R.drawable.fat_bird_px_colored), 80, 57, 1);
        birds = new ArrayList<Bird>();
        balls=new ArrayList<Paintball>();
        birdStartTime=System.nanoTime();
        thread = new MainThread(getHolder(), this);
        //can safely start the game loop
        thread.setRunning(true);
        thread.start();

    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        Rect red = new Rect((int)(13*getWidth()/WIDTH), (int)(20*getHeight()/HEIGHT), (int)(80*getWidth()/WIDTH), (int)(100*getHeight()/HEIGHT));
        Rect blue = new Rect((int)(13*getWidth()/WIDTH), (int)(108*getHeight()/HEIGHT), (int)(80*getWidth()/WIDTH), (int)(188*getHeight()/HEIGHT));
        Rect yellow = new Rect((int)(13*getWidth()/WIDTH), (int)(196*getHeight()/HEIGHT), (int)(80*getWidth()/WIDTH), (int)(276*getHeight()/HEIGHT));
        Rect green = new Rect((int)(13*getWidth()/WIDTH), (int)(284*getHeight()/HEIGHT), (int)(80*getWidth()/WIDTH), (int)(364*getHeight()/HEIGHT));
        Rect purple = new Rect((int)(13*getWidth()/WIDTH), (int)(372*getHeight()/HEIGHT), (int)(80*getWidth()/WIDTH), (int)(452*getHeight()/HEIGHT));
        Rect mainscreen = new Rect((int)(90*getWidth()/WIDTH), (int)(0*getHeight()/HEIGHT), (int)(856*getWidth()/WIDTH), (int)(480*getHeight()/HEIGHT));

        if(event.getAction()==MotionEvent.ACTION_DOWN){
            if(!player.getPlaying()&&newGameCreated&&reset)
            {
                player.setPlaying(true);
                numberofBirds=0;
                lives=3;
                gameover=0;
                for(int i=0; i<4; i++) {
                    memoryTime = System.nanoTime();
                    while (System.nanoTime() < memoryTime + 2000000000) {
                        memory = true;
                    }
                }
                memory=false;

                //player.setUp(true);
            }
            else
            {
                if(player.getPlaying()&&!memory)
                {
                    if(!started)started=true;
                    reset=false;
                    player.setUp(true);

                    if(red.contains((int)event.getX(), (int)event.getY()))
                    {
                        color =0;
                        System.out.println("RED");
                    }

                    if(blue.contains((int)event.getX(), (int)event.getY()))
                    {
                        color=1;
                    }

                    if(yellow.contains((int)event.getX(), (int)event.getY()))
                        color=2;
                    if(green.contains((int)event.getX(), (int)event.getY()))
                        color=3;
                    if(purple.contains((int)event.getX(), (int)event.getY()))
                        color=4;
                    if(mainscreen.contains((int)event.getX(), (int)event.getY()))
                    {
                        balls.add(new Paintball(BitmapFactory.decodeResource(getResources(), R.drawable.paintball), 160, player.getY()+17, color, 1));
                    }


                }

                return true;
            }
            }

        if(event.getAction()==MotionEvent.ACTION_UP)
        {
            player.setUp(false);
            return true;
        }
        return super.onTouchEvent(event);
    }

    public boolean collision(GameObject a, GameObject b)
    {
        if (Rect.intersects(a.getRectangle(), b.getRectangle()))
        {
            return true;
        }

        return false;
    }

    public boolean sameColor(GameObject a, GameObject b)
    {
        //Birds is object b
        int birdTy = b.getBirdType();
        int birdColor = birds.get(birdTy).getColor()-1;
        if (a.getColor()==birdColor)
        {
            return true;
        }

        return false;
    }



    public void update()
    {
        if(lives==0)
        {
            player.setPlaying(false);
            //memory=true;
            onetime=0;
            reset=true;
            newGameCreated=true;
            birds.clear();
            balls.clear();
            level++;
            score=0;
            gameover++;
        }


        if(player.getPlaying()&&!memory)
        {

            bg.update();
            palette.update();
            player.update();

            long birdtiming = (System.nanoTime()-birdStartTime)/1000000;
            if(birdtiming > 4000&&birds.size()<birdTypes.length+2 && numberofBirds<3)
            {
                int birdTy =rand.nextInt(birdTypes.length);
                birds.add(new Bird(BitmapFactory.decodeResource(getResources(), birdTypes[birdTy]), rand.nextInt(580)+ 190, rand.nextInt(380), 85, 65, 0, birdTy, 3));
                birdStartTime=System.nanoTime();
                numberofBirds++;
            }

            for(int i=birdTypes.length; i<birds.size(); i++)
                birds.get(i).update();

            for(int i=0; i<balls.size(); i++)
            {
                balls.get(i).update();
                for (int j=birdTypes.length; j<birds.size(); j++)
                {
                    if(collision(balls.get(i), birds.get(j)))
                    {
                        if(sameColor(balls.get(i), birds.get(j)))
                        {
                            balls.remove(i);
                            birds.remove(j);
                            score++;
                            //break;
                        }
                        else
                        {
                            balls.remove(i);
                            score--;
                            lives--;
                            if(score<0)
                                score=0;
                        }

                        if(birds.size()==birdTypes.length&&numberofBirds==3)
                        {
                            player.setPlaying(false);
                            //memory=true;
                            onetime=0;
                            reset=true;
                            newGameCreated=true;
                            birds.clear();
                            balls.clear();
                            level++;
                        }

                    }
                }

            }





        }
        else
        {
            if(onetime>0)
            {
                for(int i=0; i<birdTypes.length; i++)
                    birds.get(i).update();
            }


            player.resetDY();
            if(!reset)
            {
                newGameCreated = false;
                startReset = System.nanoTime();
                reset = true;

            }

            long resetElapsed = (System.nanoTime()-startReset)/1000000;

            if(resetElapsed > 2500 && !newGameCreated)
            {
                newGame();
            }

        }

        if(memory&&onetime==0)
        {
            ArrayList<Integer> birdcolor = new ArrayList<Integer>();
            while(birdcolor.size()<birdTypes.length){
                int random = rand.nextInt(5)+1;
                if(!birdcolor.contains(random))
                    birdcolor.add(random);
            }
            /*
            ArrayList<Integer> locx = new ArrayList<Integer>();
            ArrayList<Integer> locy = new ArrayList<Integer>();
            ArrayList<Rect> birdLoc = new ArrayList<Rect>();
            while(birdLoc.size()<birdTypes.length){
                int randomx = rand.nextInt(580)+ 190;
                int randomy = rand.nextInt(380);
                Rect birdLoctemp= new Rect(randomx, randomy, randomx+85, randomy+65);
                if(!birdLoc.contains(randomx, randomy))
                {
                    birdLoc.add(birdLoctemp);
                    locx.add(randomx);
                    locy.add(randomy);
                }


            }*/

            ArrayList<Integer> locx = new ArrayList<Integer>();
            ArrayList<Point> widex = new ArrayList<Point>();
            while(locx.size()<birdTypes.length) {
                int randomx = rand.nextInt(580) + 190;
                if (!widex.contains(randomx)) {
                    widex.add(new Point(randomx, randomx + 65));
                    locx.add(randomx);
                }
            }


                for(int i=0; i<birdTypes.length; i++)
            {
                int color=(birdcolor.get(i)%5)+1;
                birds.add(new Bird(BitmapFactory.decodeResource(getResources(), birdTypes[i]), locx.get(i), rand.nextInt(380), 85, 65, color, i, 3));
            }
            onetime++;
        }

    }
    @Override
    public void draw(Canvas canvas)
    {
        final float scaleFactorX= getWidth()/(WIDTH*1.f);
        final float scaleFactorY=getHeight()/(HEIGHT*1.f);
        if(canvas!=null){
            final int savedState= canvas.save();
            canvas.scale(scaleFactorX, scaleFactorY);
            bg.draw(canvas);
            palette.draw(canvas);
            player.draw(canvas);
            drawText(canvas);
            if(memory) {
               // for (Bird br : birds) {
               //     br.draw(canvas);
               // }
                for(int i=0; i<birdTypes.length; i++)
                    birds.get(i).draw(canvas);

            }
                if (!memory)
                {
                    for(int i=birdTypes.length; i<birds.size(); i++)
                        birds.get(i).draw(canvas);

                    for(Paintball pb: balls)
                        pb.draw(canvas);
                }
            //bird.draw(canvas);

            canvas.restoreToCount(savedState);
        }


    }

    public void newGame()
    {
        //player.resetDY();
        //player.resetScore();
        player.setY(HEIGHT/2);

        if(player.getScore()>best)
        {
            best = player.getScore();

        }

        newGameCreated = true;

    }

    public void drawText(Canvas canvas)
    {
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setTextSize(30);
        paint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        canvas.drawText("SCORE: " + score, 100, HEIGHT - 10, paint);
        if(lives==3)
        canvas.drawText("LIVES: * * *", WIDTH - 215, HEIGHT - 10, paint);
        if(lives==2)
            canvas.drawText("LIVES: * * ", WIDTH - 215, HEIGHT - 10, paint);
        if(lives==1)
            canvas.drawText("LIVES: *", WIDTH - 215, HEIGHT - 10, paint);

        if(!player.getPlaying()&&newGameCreated&&reset&&!memory)
        {
            Paint paint1 = new Paint();
            paint1.setTextSize(40);
            paint1.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

            if(score>0)
            {
                canvas.drawText("YOUR SCORE: " + score, WIDTH/2-50, HEIGHT/2-40, paint1);
                canvas.drawText("PRESS TO CONTINUE", WIDTH/2-50, HEIGHT/2, paint1);
            }
            else
                canvas.drawText("PRESS TO START", WIDTH/2-50, HEIGHT/2, paint1);

            if(gameover>0)
                canvas.drawText("GAME OVER", WIDTH/2-50, HEIGHT/2-40, paint1);

            paint1.setTextSize(20);
            canvas.drawText("RETURN THE COLOR TO THE GRAYBIRDS", WIDTH/2-50, HEIGHT/2 + 20, paint1);
            canvas.drawText("PICK THE COLOR FROM THE PALETTE", WIDTH/2-50, HEIGHT/2 + 40, paint1);
            canvas.drawText("AND TAP THE SCREEN TO SHOOT", WIDTH/2-50, HEIGHT/2 + 60, paint1);
        }
    }

    /*
    public void Memoria(Canvas canvas)
    {
        if(player.getPlaying()&&memory) {
            for(int i=0; i<4; i++) {
                memoryTime = System.nanoTime();
                while (System.nanoTime() < memoryTime + 2000000000) {
                    bird.draw(canvas);
                }
            }
            memory=false;

        }
    }*/


}
