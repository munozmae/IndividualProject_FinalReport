package com.example.munozmae.myfirstapp;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * Created by munozmae on 23/11/2016.
 */

public class Palette {
    private Bitmap image;
    private int x, y;

    public Palette(Bitmap res)
    {
        image = res;
    }
    public void update()
    {

    }
    public void draw(Canvas canvas)
    {
        canvas.drawBitmap(image, x, y, null);
    }

}
