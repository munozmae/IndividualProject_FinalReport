package com.example.munozmae.myfirstapp;

import android.graphics.Rect;

/**
 * Created by munozmae on 17/11/2016.
 */

public abstract class GameObject {
    protected int x;
    protected int y;
    protected int dy;
    protected int dx;
    protected int width;
    protected int height;
    protected double time;
    protected int color;
    protected int birdType;

    public void setX(int x)
    {
        this.x=x;
    }

    public void setY(int y)
    {
        this.y=y;
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }

    public int getWidth()
    {
        return width;
    }

    public int getHeight()
    {
        return height;
    }

    public Rect getRectangle()
    {
        return new Rect(x, y, x+width, y+height);
    }

    public int getColor() { return color;}

    public int getBirdType() { return birdType;}

}

