package com.example.munozmae.myfirstapp;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import java.util.Random;

/**
 * Created by munozmae on 23/11/2016.
 */

public class Paintball extends GameObject {
    private Bitmap spritesheet;
    private Animation animation = new Animation();
    private Random rand = new Random();
    private int speed;

    public Paintball(Bitmap res, int x, int y, int c, int numFrames)
    {
        super.x=x;
        super.y=y;
        dy=0;
        height = 30;
        width = 30;
        color = c;
        time = 0;
        speed=3;
        //color =rand.nextInt(5);



        Bitmap[] image = new Bitmap[numFrames];
        spritesheet=res;

        for (int i=0; i<image.length; i++)
        {
            image[i]= Bitmap.createBitmap(spritesheet, i*width, c*height, width, height);
        }

        animation.setFrames (image);
        animation.setDelay(10);

    }

    public void draw(Canvas canvas)
    {
        canvas.drawBitmap(animation.getImage(),x,y,null);
    }
    public void update() {
        //y= yoffset+birdfly*(int)(Math.sin(time*(Math.PI/180)));
        x+=speed;
        animation.update();
        //System.out.println("time :"+time);
        //System.out.println("y :"+y);
    }

}
