package com.example.munozmae.myfirstapp;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import java.util.Random;

/**
 * Created by munozmae on 20/11/2016.
 */

public class Bird extends GameObject {
    private Bitmap spritesheet;
    private double dya;
    private boolean up;
    private boolean playing;
    private Animation animation = new Animation();
    private long startTime;
    private Random rand = new Random();
    private int birdfly;
    private int yoffset;

    public Bird(Bitmap res, int x, int y, int w, int h, int c, int n, int numFrames)
    {
        super.x=x;
        super.y=y;
        dy=0;
        height = h;
        width = w;
        time = 0;
        color =c;
        birdfly=480-y-65-10;
        yoffset=y;
        birdType = n;


        Bitmap[] image = new Bitmap[numFrames];
        spritesheet=res;

        for (int i=0; i<image.length; i++)
        {
            image[i]= Bitmap.createBitmap(spritesheet, i*width, color*65, width, height);
        }

        animation.setFrames (image);
        animation.setDelay(50);
        startTime = System.nanoTime();

    }

    public void draw(Canvas canvas)
    {
        canvas.drawBitmap(animation.getImage(),x,y,null);
    }
    public void update() {
        //y= yoffset+birdfly*(int)(Math.sin(time*(Math.PI/180)));
        y= yoffset+(int)(20*Math.sin(time*(Math.PI/180)));
        time++;
        animation.update();
        //System.out.println("time :"+time);
        //System.out.println("y :"+y);
    }
}
